#include "../include/TextureManager.hpp"

#include "../include/spdlog/spdlog.h"

TextureManager::TextureManager(){ }
TextureManager::~TextureManager(){
	spdlog::info("Cleaning textures.");
	for(auto &t : m_Textures){
		delete t.second;
	}
	m_Textures.clear();
	resource_dirs.clear();
}

sf::Texture *TextureManager::GetTexture(std::string name){

	sf::Texture *tex = new sf::Texture();

	if(m_Textures.find(name) == m_Textures.end()){

		if(tex->loadFromFile(name)){
			m_Textures[name] = tex;
			return m_Textures[name];
		}

		spdlog::warn("Did not get direct image {0}.. trying directories..", name);

		for(auto it = resource_dirs.begin(); it != resource_dirs.end(); ++it){
			if(tex->loadFromFile(*it + name)){
				m_Textures[name] = tex;
				return m_Textures[name];
			}
		}
	}else{
		return m_Textures[name];
	}

	spdlog::error("Did not find specified texture ( {0} ) using default", name);

	sf::Image img;
	img.create(64,64);
	img.createMaskFromColor(sf::Color::Black);

	tex->loadFromImage(img);
	m_Textures[name] = tex;

	return m_Textures[name];
}

void TextureManager::SetResourceDir(std::string dir){

	for(auto it = resource_dirs.begin(); it != resource_dirs.end(); ++it){
		if(*it == dir){
			spdlog::warn("Specified directory {0} already exists. Aborting..", dir);
			return;
		}
	}

	resource_dirs.push_back(dir);
}