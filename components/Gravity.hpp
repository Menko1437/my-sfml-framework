#ifndef GRAVITY_HPP
#define GRAVITY_HPP

#include <entityx/entityx.h>
#include <memory>

struct Gravity : entityx::Component<Gravity>{
    Gravity(float amount) : ampliture(amount){ }
    Gravity() : ampliture(1.0f) {}
    float ampliture;
};


#endif // !GRAVITY_HPP