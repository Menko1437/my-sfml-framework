#ifndef VELOCITY_HPP
#define VELOCITY_HPP

#include <entityx/entityx.h>

#include "../math/Vectors.hpp"

struct VelocityComponent : entityx::Component<VelocityComponent>{
    VelocityComponent(float x = 0.0f, float y = 0.0f) : velocity {x,y} { }

    Vec2Df velocity;
};


#endif // !VELOCITY_HPP