#ifndef INTRO_HPP
#define INTRO_HPP

// std
#include <iostream>

// 3rd party
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// local
#include "../include/GameState.hpp"

class Intro : public GameState{

public:
	void Init(Core *game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(Core *game);
	void Update(Core *game);
	void Draw(Core *game);

	static Intro* Instance() {
		return &m_IntroState;
	}

protected:
	Intro() { }

private:
	static Intro m_IntroState;
    sf::Event m_Event;
    sf::Font m_Font;
    sf::Text m_Text;

	//gui
	tgui::Gui gui;
	tgui::Button::Ptr button = tgui::Button::create();
};

#endif // !INTRO_HPP

