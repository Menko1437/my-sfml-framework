#include "../states/Settings.hpp"
#include "../states/MainMenu.hpp"
#include <fstream>
#include <regex>
#include <string>

#include "../include/spdlog/spdlog.h"

Settings Settings::m_SettingsState;

void Settings::Init(Core *game){
    std::cout << "Initializing Settings.." << std::endl;
    m_Event = std::make_unique<sf::Event>();
    clock = std::make_unique<sf::Clock>();

    gui = std::make_unique<tgui::Gui>();
    gui->setTarget(game->getWindow());

    // regex
    std::regex fpsString("fps: ([0-9]{2,3})");
    std::regex aaString("aalias: ([0-8])");
    std::regex vsyncString("vsync: ([0|1])");
    std::smatch match;

    std::ifstream m_File;
    m_File.open("../config/settings.txt");
    if(m_File.good()){
        std::string line;
        while (getline(m_File,line)){
            if(std::regex_search(line, match, fpsString)){
                m_CurrentFPS = std::stoi(match[1].str());
            }
            if(std::regex_search(line, match, aaString)){
                m_CurrentAA = std::stoi(match[1].str());
            }
            if(std::regex_search(line, match, vsyncString)){
                if(match[1].str() == "1"){ 
                    m_cbVsync->setChecked(true);
                }else{
                    m_cbVsync->setChecked(false);
                }
            }
        }
    }
    m_File.close();

    m_lblAntiAliasing->setPosition(game->getWindowSize().x / 2 - 250, game->getWindowSize().y / 2 - 50);
    m_lblAntiAliasing->setTextSize(20);
    m_lblAntiAliasing->setText(" Anti Aliasing :");
    gui->add(m_lblAntiAliasing);

    m_lblAntiAliasingCurrent->setPosition(game->getWindowSize().x / 2 + 100, game->getWindowSize().y / 2 - 50);
    m_lblAntiAliasingCurrent->setTextSize(15);
    gui->add(m_lblAntiAliasingCurrent);

    m_sAntiAliasing->setPosition(game->getWindowSize().x / 2 - 50, game->getWindowSize().y / 2 - 50);
    m_sAntiAliasing->setSize(150,20);
    m_sAntiAliasing->setMinimum(0);
    m_sAntiAliasing->setMaximum(8);
    m_sAntiAliasing->setValue(m_CurrentAA);
    m_sAntiAliasing->setStep(4);
    gui->add(m_sAntiAliasing);

    m_lblFpsShow->setPosition(20,20);
    m_lblFpsShow->setTextSize(20);
    gui->add(m_lblFpsShow);

    m_lblFps->setPosition(game->getWindowSize().x / 2 - 155, game->getWindowSize().y / 2);
    m_lblFps->setText("FPS :");
    m_lblFps->setTextSize(20);
    gui->add(m_lblFps);

    m_sFps->setPosition( game->getWindowSize().x / 2 - 50, game->getWindowSize().y / 2 + 5);
    m_sFps->setSize(150,20);
    m_sFps->setMinimum(30);
    m_sFps->setMaximum(144);
    m_sFps->setValue(m_CurrentFPS);
    m_sFps->setStep(1);
    gui->add(m_sFps);

    m_lblFpsCurrent->setPosition(game->getWindowSize().x / 2 + 130, game->getWindowSize().y / 2 + 5);
    m_lblFpsCurrent->setText(std::to_string(m_CurrentFPS));
    m_lblFpsCurrent->setTextSize(15);
    gui->add(m_lblFpsCurrent);

    m_cbVsync->setPosition(game->getWindowSize().x / 2 - 50, game->getWindowSize().y / 2 + 50 + 5);
    gui->add(m_cbVsync);

    m_lblVsync->setPosition(game->getWindowSize().x / 2 - 170, game->getWindowSize().y / 2 + 50);
    m_lblVsync->setText("Vsync ");
    m_lblVsync->setTextSize(20);
    gui->add(m_lblVsync);

    m_backButton->setPosition(game->getWindowSize().x - 150, game->getWindowSize().y - 50);
    m_backButton->setSize(100,30);
    m_backButton->setText("back");
    gui->add(m_backButton);

    m_sFps->connect("ValueChanged", [&](){ m_FpsChanged = true; });
    m_backButton->connect("pressed", [=](){ game->PopState();});
    m_sAntiAliasing->connect("ValueChanged", [&]() { m_AAChanged = true; });
}
 


void Settings::Cleanup(){
    spdlog::info("Saving settings.");

    std::ofstream m_File;
    m_File.open("../config/settings.txt");
    std::string vs = m_cbVsync->isChecked() ? "1" : "0";
    m_File << "fps: " << m_CurrentFPS << "\n" << "aalias: " << m_CurrentAA << "\n" << "vsync: " << vs << "\n";
    m_File.close();
    
    spdlog::info("Settings Cleanup..");
}

void Settings::Pause(){

}

void Settings::Resume(){

}

void Settings::HandleEvents(Core *game){
    while(game->getWindow().pollEvent(*m_Event)){
        if (m_Event->type == sf::Event::Closed){
            game->Quit();
        }
            
        if(m_Event->type == sf::Event::Resized){
            gui->setView(game->getWindow().getView());
        }
        
        if(m_FpsChanged){
            game->getWindow().setFramerateLimit(m_sFps->getValue());
            m_FpsChanged = false;  
        }

        if(m_cbVsync->isChecked()){
            game->getWindow().setVerticalSyncEnabled(true);
        }else{
            game->getWindow().setVerticalSyncEnabled(false);
        }

        if(m_AAChanged){
            game->getSettings().antialiasingLevel = m_CurrentAA;
            m_AAChanged = false;
        }
        
        
    gui->handleEvent(*m_Event);
    }
}

void Settings::Update(Core *game){
    m_CurrentFPS = static_cast<int>(m_sFps->getValue());
    m_CurrentAA = static_cast<int>(m_sAntiAliasing->getValue());

    m_lblFpsCurrent->setText(std::to_string(m_CurrentFPS));
    m_lblAntiAliasingCurrent->setText(std::to_string(m_CurrentAA));

    float currentTime = clock->restart().asSeconds();
    float fps = 1.f / currentTime;

    m_lblFpsShow->setText(std::to_string(fps));

}

void Settings::Draw(Core *game){

    game->getWindow().clear(sf::Color::White);

    gui->draw();
    game->getWindow().display();
}