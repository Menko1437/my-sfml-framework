#include "../states/Intro.hpp"
#include "../include/Core.hpp"
#include "../states/MainMenu.hpp"

#include "../include/spdlog/spdlog.h"

Intro Intro::m_IntroState;

void Intro::Init(Core *game){
    spdlog::info("Intro state intialized.");

    if(!m_Font.loadFromFile("../fonts/intro.ttf")){
        spdlog::error("Failed to load font: ../fonts/intro.ttf");
    }else{
    m_Text.setFont(m_Font);
    m_Text.setString("Nagarus Framework");
    m_Text.setPosition((game->getWindowSize().x / 2) - 100, (game->getWindowSize().y / 2) - 50);
    m_Text.setFillColor(sf::Color::Black);
    }

    gui.setTarget(game->getWindow());


    button->setSize(100,30);
    button->setText("Enter");
    button->setPosition((game->getWindowSize().x / 2) - 50, game->getWindowSize().y / 2);
    
    gui.add(button);

    button->connect("pressed", [=](){ game->ChangeState( MainMenu::Instance()); });
}
void Intro::Cleanup(){
    spdlog::info("Intro state cleaned.");
}

void Intro::Pause(){
    spdlog::info("Intro state paused.");
}
void Intro::Resume(){
    spdlog::info("Intro state resumed.");
}

void Intro::HandleEvents(Core* game){
    while(game->getWindow().pollEvent(m_Event)){
            if (m_Event.type == sf::Event::Closed){
                game->Quit();
            }
            
            if(m_Event.type == sf::Event::Resized){
                gui.setView(game->getWindow().getView());
            }

            gui.handleEvent(m_Event);
    }
}
void Intro::Update(Core* game){
    
}
void Intro::Draw(Core* game){
    game->getWindow().clear(sf::Color::White);
    game->getWindow().draw(m_Text);
    gui.draw();
    game->getWindow().display();
}