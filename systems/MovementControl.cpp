#include "../systems/MovementControl.hpp"

MovementControl::MovementControl(){

}

void MovementControl::update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt){
    for (auto entity : es.entities_with_components<PositionComponent, VelocityComponent>()){
        entityx::ComponentHandle<PositionComponent> pos = entity.component<PositionComponent>();
        entityx::ComponentHandle<VelocityComponent> vel = entity.component<VelocityComponent>();

        // X velocity
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
            vel.get()->velocity.x = -100;
        }else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
            vel.get()->velocity.x = 100;
        }else{
            vel.get()->velocity.x = 0;
        }

        // currently dissable moving UP
        // Y velocity
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
            //vel.get()->velocity.y = -100;
        }else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
            vel.get()->velocity.y = 100;
        }else{
            vel.get()->velocity.y = 0;
        }
        

        pos.get()->position.x += vel.get()->velocity.x * dt;
        pos.get()->position.y += vel.get()->velocity.y * dt;
    }
}