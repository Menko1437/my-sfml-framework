#ifndef MOVEMENTCONTROL_HPP
#define MOVEMENTCONTROL_HPP

#include <entityx/entityx.h>
#include <SFML/Graphics.hpp>

#include "../components/Position.hpp"
#include "../components/Velocity.hpp"

class MovementControl : public entityx::System<MovementControl>{
public:
    MovementControl();
    void update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt) override;
private:

};


#endif // !MOVEMENTCONTROL_HPP