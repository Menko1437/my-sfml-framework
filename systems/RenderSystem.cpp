#include "../systems/RenderSystem.hpp"

#include <SFML/Graphics.hpp>

#include "../include/TextureManager.hpp"

#include "../components/Position.hpp"
#include "../components/Renderable.hpp"

void RenderSystem::update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt){
    for (auto entity : es.entities_with_components<Renderable>()){
        entityx::ComponentHandle<Renderable> renderable = entity.component<Renderable>();
        entityx::ComponentHandle<PositionComponent> pos = entity.component<PositionComponent>();

        renderable->sprite.setTexture(renderable->texture);
        renderable->sprite.setPosition(sf::Vector2f(pos->position.x,pos->position.y));

        renderable->target.draw(renderable->sprite);
    }
}