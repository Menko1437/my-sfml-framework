# Nagarus framework

## Features:
- [x] state manager
- [x] gui (tgui)
- [x] texture manager
- [x] audio manager
- [x] animation
- [x] Entity component system (EntityX)


## Dependencies:
* SFML 2.5+
* TGUI 
* EntityX

## How it works
```mermaid
    graph TD;
    Main-->Core;
    StateMgr-->Core;
    Core-->MainMenu;
    MainMenu-->Settings;
    MainMenu-->Exit;
    MainMenu-->Start;
    TextureMgr-->Start;
    AudioMgr-->Start;
    ECS-->Core;
```

## building

Nagarus framework uses meson build system.
In main directory type
`meson --reconfigure build`

Next step is going to build directory,
and typing `ninja`.
