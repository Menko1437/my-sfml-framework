#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include "../include/Core.hpp"


class GameState{
public:

    virtual void Init(Core *app) = 0;
    virtual void Cleanup() = 0;

    virtual void Pause() = 0;
    virtual void Resume() = 0;

    virtual void HandleEvents(Core *app) = 0;
    virtual void Update(Core *app) = 0;
    virtual void Draw(Core *app) = 0;


    void ChangeState(Core *app,GameState *state) {
        app->ChangeState(state);
    }

    virtual ~GameState(){ }

  protected: 
    GameState() { }
};

#endif // !GAMESTATE_HPP
