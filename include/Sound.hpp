#ifndef SOUND_HPP
#define SOUND_HPP

// std
#include <iostream>

// 3rd party
#include <SFML/Audio.hpp>
// local

class Sound{
public:
    Sound();
    ~Sound();

    bool loadFromFile(const std::string &filename);

    std::unique_ptr<sf::SoundBuffer> buffer;
    std::unique_ptr<sf::Sound> sound;

private:

};


#endif // !SOUND_HPP