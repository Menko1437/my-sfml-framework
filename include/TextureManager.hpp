#ifndef TEXTUREMANAGER_HPP
#define TEXTUREMANAGER_HPP

// std
#include <unordered_map>
#include <vector>
#include <iostream>

// 3rdparty
#include <SFML/Graphics.hpp>

//local


class TextureManager{
public:

	TextureManager();
   ~TextureManager();

   sf::Texture *GetTexture(std::string name);
   void SetResourceDir(std::string dir);

private:
	std::unordered_map<std::string, sf::Texture*> m_Textures;
	std::vector<std::string> resource_dirs;
};
  

#endif // !TEXTUREMANAGER_HPP