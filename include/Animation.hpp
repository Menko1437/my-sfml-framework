#ifndef Animation_HPP
#define Animation_HPP

// std
#include <iostream>
#include <map>
#include <chrono>
#include <thread>


// 3rd party
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/Vector2.hpp>

// local
#include "../include/GameState.hpp"


class Animation{
public:
    Animation(const sf::Texture *texture,sf::Vector2u imageCount,float switchTime);
    ~Animation();

    void Update(int row,float dt, bool faceLeft);
    
    sf::IntRect m_SpriteRect;

private: 

    sf::Vector2u m_ImageCount;
    sf::Vector2u m_CurrentImage;

    float m_TotalTime;
    float m_SwitchTime;

};

#endif // !Animation_HPP
